# LaTeX templates for Eskild Hustvedt

## hib-paper.tex

A LaTeX paper template that fits the style specifications for HiB (Høgskolen i
Bergen/Bergen University College). Citation and bibliography fits the Harvard
style as presented on [Søk og
skriv](http://sokogskriv.no/kildebruk-og-referanser/referansestiler/harvard/).
Requires XeTeX, and uses biblatex instead of bibtex. It overrides the default
LaTeX fonts and margins, and has a custom header, which makes it fit the HiB
style guidelines closely.

## hib-paper-complete/
This directory contains a ready-to-use project that based upon the
hib-paper.tex style, as well as a HiB-style titlepage. In most cases this will
be easier and faster to start using than hib-paper.tex is.

## hib-paper-light.tex

A version of hib-paper.tex that does not follow the style guidelines as
closely. This version does not override the fonts and margins, and has a more
standard LaTeX header. This may look better, and works for papers where the
style guidelines are not as strict.

## contract.tex

A very basic contract template

## letter.tex

A basic Norwegian letter template

## paper.tex

A basic template for writing papers

## specifications.tex

A basic template for writing project specifications

## word-processor-like.tex

A template for writing documents that look like they're made in a word
processor like LibreOffice. This is for situations where there are strict
requirements on how a document should look. Requires XeTeX.

## License

[![CC 0](http://i.creativecommons.org/p/zero/1.0/88x31.png)](http://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, Eskild Hustvedt has waived all copyright and
related or neighboring rights to this work. This work is published from:
Norway.
